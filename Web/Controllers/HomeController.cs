﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TRPO_Lab3.Libr;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Privacy()
        {            
            return View();
        }


        public ActionResult Calc()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Calc(string a, string b, string c)
        {
            ViewBag.result = Profit.Gera(Convert.ToDouble(a), Convert.ToDouble(b), Convert.ToDouble(c));
            return View();
        }
    }
}
