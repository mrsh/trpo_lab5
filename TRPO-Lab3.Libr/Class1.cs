﻿using System;

namespace TRPO_Lab3.Libr
{
    public class Profit
    {
        public static double Gera(double a, double c, double b)
        {
            double P = (a + b + c) / 2;
            return Math.Sqrt(P * (P - a) * (P - b) * (P - c));
        }
    }



}
